import { Sequence } from './sequencers/Sequence.ts'
import { InAndOut } from './sequencers/InAndOut.ts'
import { Toggle } from './sequencers/Toggle.ts'
import { Solid } from './sequencers/Solid.ts'
import { RockPaperScissorsLizardSpock } from './sequencers/RockPaperScissorsLizardSpock.ts'
import { LegFader } from './sequencers/LegFader.ts'
import { Lap } from './sequencers/Lap.ts'
import { LegLine } from './sequencers/LegLine.ts'
import { Sparkle } from './sequencers/Sparkle.ts'
import { Twirl } from './sequencers/Twirl.ts'

export class Star {
    #leds: Array<string>
    #currentSequence?: Sequence
    /** Which sequence to use.  undefined will randomly pick a sequence. */
    #sequence?: typeof Sequence

    constructor(sequence?: typeof Sequence) {
        this.#leds = []
        this.#sequence = sequence
        this.tick(0)
    }

    public get leds(): Array<string> {
        return this.#leds
    }

    /**
     * Return an integer in the range [from, to]
     *
     * @param from - inclusive
     * @param to - inclusive
     * @returns
     */
    static randomInRange(from: number, to: number): number {
        const min = Math.min(from, to);
        const max = Math.max(from, to);
        return Math.floor(Math.random() * (max - min + 1) + min)
    }

    tick(millis: number) {
        if (this.#currentSequence) {
            if (!this.#currentSequence.tick(millis)) { // sequence has finished
                if (this.#currentSequence instanceof Solid && this.#currentSequence.color === 'black') { // End of 'transition' sequence
                    this.#currentSequence = undefined
                } else {  // Turn off lights between sequences.
                    this.#currentSequence = new Solid(this.#leds)
                }
            }
        } else {
            const r = this.#sequence ? Number.MAX_SAFE_INTEGER : Math.random()

            const N = 9

            if (this.#sequence === Toggle || r < 1 / N) {
                this.#currentSequence = new Toggle(
                    this.#leds,
                    Star.randomInRange(5, 10),
                    Star.randomInRange(200, 2000)
                )
            } else if (this.#sequence === InAndOut || r < 2 / N) {
                this.#currentSequence = new InAndOut(
                    this.#leds,
                    Star.randomInRange(1, 5),
                    Star.randomInRange(250, 3000),
                    ['IN', 'OUT', 'INOUT', 'OUTIN'][Star.randomInRange(0, 3)] as 'OUT' | 'IN' | 'INOUT' | 'OUTIN'
                )
            } else if (this.#sequence === RockPaperScissorsLizardSpock || r < 3 / N) {
                this.#currentSequence = new RockPaperScissorsLizardSpock(this.#leds)
            } else if (this.#sequence === Solid || r < 4 / N) {
                this.#currentSequence = new Solid(
                    this.#leds,
                    Star.randomInRange(1, 5),
                    Star.randomInRange(500, 2500),
                    Star.randomInRange(0, 359)
                )
            } else if (this.#sequence === Lap || r < 5 / N) {
                const speed = Star.randomInRange(1000, 5000)
                const fade = Star.randomInRange(speed / 10, speed)
                this.#currentSequence = new Lap(
                    this.#leds,
                    Star.randomInRange(1, 10),
                    speed,
                    fade
                )
            } else if (this.#sequence === LegLine || r < 6 / N) {
                const speed = Star.randomInRange(20, 100)
                const fade = Star.randomInRange(speed * 5, speed * 6 * 3)
                this.#currentSequence = new LegLine(
                    this.#leds,
                    Star.randomInRange(2, 2),
                    fade,
                    speed
                )
            } else if (this.#sequence === Sparkle || r < 7 / N) {
                this.#currentSequence = new Sparkle(this.#leds)
            } else if (this.#sequence === Twirl || r < 8 / N) {
                const speed = Star.randomInRange(50, 500)
                const fade = Star.randomInRange(speed * 2, speed * 5)
                this.#currentSequence = new Twirl(
                    this.#leds,
                    Star.randomInRange(3, 10),
                    fade,
                    speed
                )
            } else {
                this.#currentSequence = new LegFader(this.#leds)
            }
        }
    }
}
