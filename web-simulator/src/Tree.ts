import { Sequence } from './sequencers/Sequence.ts'
import { SimpleTree } from './sequencers/SimpleTree.ts';
import { Solid } from './sequencers/Solid.ts'

export class Tree {
    #leds: Array<string>
    #currentSequence?: Sequence
    /** Which sequence to use.  undefined will randomly pick a sequence. */
    #sequence?: typeof Sequence

    constructor(sequence?: typeof Sequence) {
        this.#leds = []
        this.#sequence = sequence
        this.tick(0)
    }

    public get leds(): Array<string> {
        return this.#leds
    }

    /**
     * Return an integer in the range [from, to]
     *
     * @param from - inclusive
     * @param to - inclusive
     * @returns
     */
    static randomInRange(from: number, to: number): number {
        const min = Math.min(from, to);
        const max = Math.max(from, to);
        return Math.floor(Math.random() * (max - min + 1) + min)
    }

    tick(millis: number) {
        if (this.#currentSequence) {
            if (!this.#currentSequence.tick(millis)) { // sequence has finished
                if (this.#currentSequence instanceof Solid && this.#currentSequence.color === 'black') { // End of 'transition' sequence
                    this.#currentSequence = undefined
                } else {  // Turn off lights between sequences.
                    this.#currentSequence = new Solid(this.#leds)
                }
            }
        } else {
            const r = this.#sequence ? Number.MAX_SAFE_INTEGER : Math.random()

            const N = 2

            if (this.#sequence === Solid || r < 2 / N) {
                this.#currentSequence = new Solid(
                    this.#leds,
                    Tree.randomInRange(1, 5),
                    Tree.randomInRange(500, 2500),
                    Tree.randomInRange(0, 359)
                )
            } else if (this.#sequence === SimpleTree || r < 2 / N) {
                this.#currentSequence = new SimpleTree(this.#leds)
            }
        }
    }
}
