/// <reference lib='dom' />
export class ArduinoStarSimulator {
    #canvas: HTMLCanvasElement
    #ctx: CanvasRenderingContext2D
    #title: string

    static readonly #maxCanvasCircleRadius = 190
    static readonly #canvasSize = 400
    static readonly #canvasLedSize = 10

    constructor(title = '') {
        this.#canvas = document.createElement('canvas')
        this.#ctx = this.#canvas.getContext('2d')!

        this.#canvas.height = this.#canvas.width = ArduinoStarSimulator.#canvasSize
        this.#ctx.textBaseline = 'top'

        this.#title = title

        document.body.appendChild(this.#canvas)
    }

    draw(leds: Array<string>) {
        this.#ctx.fillStyle = '#111'
        this.#ctx.font = '10px monospace'
        this.#ctx.fillRect(0, 0, ArduinoStarSimulator.#canvasSize, ArduinoStarSimulator.#canvasSize)

        for (let i = 0; i < 50; ++i) {
            this.#ctx.fillStyle = leds[i]
            const radians = 7.2 * i * Math.PI / 180  // 7.2 angle between subsequent lights
            let radius = 0
            if (i % 10 === 0) {
                radius = ArduinoStarSimulator.#maxCanvasCircleRadius
            } else if (i % 5 === 0) {
                radius = ArduinoStarSimulator.#maxCanvasCircleRadius - 100
            } else if (i % 5 === i % 10) {
                radius = ArduinoStarSimulator.#maxCanvasCircleRadius - 20 * (i % 5)
            } else {
                radius = ArduinoStarSimulator.#maxCanvasCircleRadius - 20 * (5 - (i % 5))
            }
            const x = radius * Math.sin(radians) + (ArduinoStarSimulator.#canvasSize / 2)
            const y = (ArduinoStarSimulator.#canvasSize / 2) - radius * Math.cos(radians)
            this.#ctx.fillRect(
                x,
                y,
                ArduinoStarSimulator.#canvasLedSize,
                ArduinoStarSimulator.#canvasLedSize
            )

            this.#ctx.fillStyle = 'black'
            this.#ctx.fillText(i.toString(), x, y)
        }

        // Print Sequence Title
        this.#ctx.fillStyle = 'white';
        this.#ctx.font = '14px sans-serif';
        this.#ctx.fillText(this.#title, 0, 0);
    }
}