/// <reference lib='dom' />
export class ArduinoTreeSimulator {
    #canvas: HTMLCanvasElement
    #ctx: CanvasRenderingContext2D
    #title: string

    static readonly #maxCanvasCircleRadius = 190
    static readonly #canvasSize = 400
    static readonly #canvasLedSize = 10

    constructor(title = '') {
        this.#canvas = document.createElement('canvas')
        this.#ctx = this.#canvas.getContext('2d')!

        this.#canvas.height = this.#canvas.width = ArduinoTreeSimulator.#canvasSize
        this.#ctx.textBaseline = 'top'

        this.#title = title

        document.body.appendChild(this.#canvas)
    }

    draw(leds: Array<string>) {
        this.#ctx.fillStyle = '#111'
        this.#ctx.font = '10px monospace'
        this.#ctx.fillRect(0, 0, ArduinoTreeSimulator.#canvasSize, ArduinoTreeSimulator.#canvasSize)
        const starRadius = 20

        // STAR
        for (let i = 0; i < 5; ++i) {
            this.#ctx.fillStyle = leds[i]

            const x = (ArduinoTreeSimulator.#canvasSize / 2) + starRadius * Math.sin(360 / 5 * i * Math.PI / 180)
            const y = starRadius * (1 - Math.cos(360 / 5 * i * Math.PI / 180))

            this.#ctx.fillRect(
                x,
                y,
                ArduinoTreeSimulator.#canvasLedSize,
                ArduinoTreeSimulator.#canvasLedSize
            )

            this.#ctx.fillStyle = 'black'
            this.#ctx.fillText(i.toString(), x, y)
        }

        // LEGS
        for (let j = 0; j < 5; ++j) {
            for (let i = 0; i < 9; ++i) {
                this.#ctx.fillStyle = leds[j * 9 + i + 5]

                const x = ArduinoTreeSimulator.#canvasSize / 2 - (2 - j) * 13 * (i * .7 + 1);
                const y = (40 * i) + (2 * starRadius + 2 * ArduinoTreeSimulator.#canvasLedSize)

                this.#ctx.fillRect(
                    x,
                    y,
                    ArduinoTreeSimulator.#canvasLedSize,
                    ArduinoTreeSimulator.#canvasLedSize
                )

                this.#ctx.fillStyle = 'black'
                this.#ctx.fillText((j * 9 + i + 5).toString(), x, y)
            }
        }

        // Print Sequence Title
        this.#ctx.fillStyle = 'white';
        this.#ctx.font = '14px sans-serif';
        this.#ctx.fillText(this.#title, 0, 0);
    }
}