import { Sequence } from './Sequence.ts';

export class RockPaperScissorsLizardSpock extends Sequence {
    constructor(leds: Array<string>) {
        super(leds)

        const p1 = Math.floor(Math.random() * 5)
        let p2: number

        do {
            p2 = Math.floor(Math.random() * 5)
        } while (p1 === p2);

        const ledMoves = [
            Sequence.TRIANGLE_N,
            Sequence.TRIANGLE_NE,
            Sequence.TRIANGLE_SE,
            Sequence.TRIANGLE_SW,
            Sequence.TRIANGLE_NW
        ]

        const rules = [
            [false, false, true, true, false],
            [true, false, false, false, true],
            [false, true, false, true, false],
            [false, true, false, false, true],
            [true, false, true, false, false]
        ]

        const winner = rules[p1][p2] ? p1 : p2
        const loser = rules[p1][p2] ? p2 : p1

        this.setLeds('orange', Sequence.TRIANGLE_N)
            .pause(350)
            .setLeds('orange', Sequence.TRIANGLE_NE)
            .setLeds('black', Sequence.TRIANGLE_N)
            .pause(350)
            .setLeds('orange', Sequence.TRIANGLE_SE)
            .setLeds('black', Sequence.TRIANGLE_NE)
            .pause(350)
            .setLeds('orange', Sequence.TRIANGLE_SW)
            .setLeds('black', Sequence.TRIANGLE_SE)
            .pause(350)
            .setLeds('orange', Sequence.TRIANGLE_NW)
            .setLeds('black', Sequence.TRIANGLE_SW)
            .pause(350)
            .setLeds('black', Sequence.TRIANGLE_NW)
            .pause(350)
            .setLeds('blue', ledMoves[winner])
            .setLeds('blue', ledMoves[loser])
            .pause(2000)
            .setLeds('black', ledMoves[winner])
            .setLeds('black', ledMoves[loser])
            .pause(500)
            .setLeds('green', ledMoves[winner])
            .setLeds('red', ledMoves[loser])
            .pause(1000)
            .setLeds('black', ledMoves[winner])
            .pause(500)
            .setLeds('green', ledMoves[winner])
            .pause(1000)
            .setLeds('black', ledMoves[winner])
            .pause(500)
            .setLeds('green', ledMoves[winner])
            .pause(1000)
    }
}
