import { Sequence } from './Sequence.ts'

export class InAndOut extends Sequence {
    constructor(leds: Array<string>, iterations = 1, duration = 1000, direction: 'OUT' | 'IN' | 'INOUT' | 'OUTIN' = 'OUT') {
        super(leds, iterations)

        const numEvents = direction.length <= 3 ? 7 : 12

        this.setLeds('red', direction.startsWith('IN') ? Sequence.RING_5 : Sequence.RING_0)
            .pause(duration / numEvents)
            .setLeds('red', direction.startsWith('IN') ? Sequence.RING_4 : Sequence.RING_1)
            .pause(duration / numEvents)
            .setLeds('white', direction.startsWith('IN') ? Sequence.RING_3 : Sequence.RING_2)
            .pause(duration / numEvents)
            .setLeds('white', direction.startsWith('IN') ? Sequence.RING_2 : Sequence.RING_3)
            .pause(duration / numEvents)
            .setLeds('blue', direction.startsWith('IN') ? Sequence.RING_1 : Sequence.RING_4)
            .pause(duration / numEvents)
            .setLeds('blue', direction.startsWith('IN') ? Sequence.RING_0 : Sequence.RING_5)
            .pause(duration / numEvents)

        if (direction.length > 3) {
            this.setLeds('black', direction.startsWith('IN') ? Sequence.RING_0 : Sequence.RING_5)
                .pause(duration / numEvents)
                .setLeds('black', direction.startsWith('IN') ? Sequence.RING_1 : Sequence.RING_4)
                .pause(duration / numEvents)
                .setLeds('black', direction.startsWith('IN') ? Sequence.RING_2 : Sequence.RING_3)
                .pause(duration / numEvents)
                .setLeds('black', direction.startsWith('IN') ? Sequence.RING_3 : Sequence.RING_2)
                .pause(duration / numEvents)
                .setLeds('black', direction.startsWith('IN') ? Sequence.RING_4 : Sequence.RING_1)
                .pause(duration / numEvents)
                .setLeds('black', direction.startsWith('IN') ? Sequence.RING_5 : Sequence.RING_0)
        } else {
            this.setLeds('black', Sequence.ALL_LEDS)
        }

        this.pause(duration / numEvents)
    }
}
