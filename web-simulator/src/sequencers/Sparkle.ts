import { Star } from '../Star.ts'
import { Sequence } from './Sequence.ts'

export class Sparkle extends Sequence {
    constructor(leds: Array<string>) {
        super(leds, 5)

        const usedLeds: Array<boolean> = []

        for (let i = 0; i < 50; ++i) {
            let l: number
            while (((l = Star.randomInRange(0, 49)) || true) && usedLeds[l]);
            usedLeds[l] = true;
            this.rampDownV(Star.randomInRange(0, 359), [l], Star.randomInRange(500, 5000))
            this.pause(Star.randomInRange(50, 100))
        }
    }
}
