import { Sequence } from './Sequence.ts'

export class LegLine extends Sequence {
    constructor(leds: Array<string>, iterations = 10, fadeMS = 2000, speedMS = 100) {
        super(leds, iterations)

        const legs = [
            Sequence.LEG_7,
            Sequence.LEG_10,
            Sequence.LEG_3,
            Sequence.LEG_6,
            Sequence.LEG_9,
            Sequence.LEG_2,
            Sequence.LEG_5,
            Sequence.LEG_8,
            Sequence.LEG_1,
            Sequence.LEG_4,
        ]

        for (let i = 0; i < legs.length; ++i) {
            for (let j = 0; j < 6; ++j) {
                this.rampDownV(36 * i, [legs[i][j]], fadeMS)
                this.pause(speedMS)
            }
        }
    }
}
