import { Sequence } from './Sequence.ts'

export class Twirl extends Sequence {
    constructor(leds: Array<string>, iterations = 10, fadeMS = 2000, speedMS = 100) {
        super(leds, iterations)

        // TODO assume/assert fadeMS > speedMS
        // TODO assume/assert what is the relation between fade and speed?

        const legs = [
            Sequence.LEG_9,
            Sequence.LEG_7,
            Sequence.LEG_5,
            Sequence.LEG_3,
            Sequence.LEG_1,
        ]

        const legs2 = [
            [...Sequence.LEG_8].reverse(),
            [...Sequence.LEG_6].reverse(),
            [...Sequence.LEG_4].reverse(),
            [...Sequence.LEG_2].reverse(),
            [...Sequence.LEG_10].reverse(),
        ]

        for (let j = 0; j < 5; ++j) {
            for (let i = 0; i < legs.length; ++i) {
                this.rampDownV(72 * i, [legs[i][j]], fadeMS)
                this.rampDownV(72 * i, [legs2[i][j]], fadeMS)
            }
            this.pause(speedMS)
        }
    }
}
