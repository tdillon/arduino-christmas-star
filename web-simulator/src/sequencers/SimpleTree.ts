import { Sequence } from './Sequence.ts';

export class SimpleTree extends Sequence {
    constructor(leds: Array<string>, iterations = 1, duration = 500) {
        super(leds, iterations)

        this.setLeds('green', Sequence.ALL_LEDS)
            .setLeds('yellow', Sequence.LEDS_TREE_STAR)
            .pause(duration)
    }
}
