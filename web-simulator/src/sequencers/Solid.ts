import { Sequence } from './Sequence.ts';

export class Solid extends Sequence {
    /**
     * @example
     * // `duration`, `iterations`, and `color` are all optional.
     * // Default is to turn all lights off for 1/2 second.
     * new Solid(this.#leds)
     *
     * @example
     * // All leds blue for a total of 6 seconds (plus fade up/down * 2).
     * new Solid(this.#leds, 3000, 2, 'blue')
     *
     * @param leds
     * @param duration - length of time (milliseconds) which the LED is 100%.  Fade up/down are additional time per iteration.
     * @param iterations
     * @param color
     */
    constructor(leds: Array<string>, iterations = 1, duration = 500, public color: 'black' | number = 'black') {
        // duration + (color === 'black' ? 0 : 2000),
        super(leds, iterations)

        if (color === 'black') {
            this.setLeds(color, Solid.ALL_LEDS)
            this.pause(duration)
        } else {
            const rampDuration = 1000
            this.rampUpV(color, Solid.ALL_LEDS, rampDuration)
            this.pause(rampDuration)
            this.setLeds(`hsl(${color}, 100%, 50%)`, Solid.ALL_LEDS)
            this.pause(duration)
            this.rampDownV(color, Solid.ALL_LEDS, rampDuration)
            this.pause(rampDuration)
        }
    }
}
