import { Sequence } from './Sequence.ts';

export class Lap extends Sequence {
    constructor(leds: Array<string>, iterations = 2, duration = 5000, tail = 1000) {
        super(leds, iterations)

        for (let i = 0; i < 50; ++i) {
            this.rampDownV(7.2 * i, [i], tail)
                .pause(duration / 50)
        }
    }
}
