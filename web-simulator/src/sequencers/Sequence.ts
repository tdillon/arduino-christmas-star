/**
 * Could a class/struct like this be used on the arduino be used
 * to store the led indices using a fixed 8 bytes instead of an
 * array of bytes (of between 1 and 50 bytes)?
 */
class BitThing {
    #a = 0
    #b = 0

    constructor(i: Array<number>) {
        this.#setIndices(i)
    }

    #setIndices(i: Array<number>) {
        for (const a of i) {
            if (a < 25) {
                this.#a |= (1 << a)
            } else {
                this.#b |= (1 << a)
            }
        }
    }

    isIndexSet(i: number): boolean {
        return (((1 << i) & this.#a) > 0) || (((1 << i) & this.#b) > 0)
    }
}

export abstract class Sequence {
    static ALL_LEDS = [
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
        10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
        20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
        30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
        40, 41, 42, 43, 44, 45, 46, 47, 48, 49,
    ]
    static RING_0 = [5, 15, 25, 35, 45] // inner most ring
    static RING_1 = [4, 6, 14, 16, 24, 26, 34, 36, 44, 46]
    static RING_2 = [3, 7, 13, 17, 23, 27, 33, 37, 43, 47]
    static RING_3 = [2, 8, 12, 18, 22, 28, 32, 38, 42, 48]
    static RING_4 = [1, 9, 11, 19, 21, 29, 31, 39, 41, 49]
    static RING_5 = [0, 10, 20, 30, 40]  // outer most ring, i.e., tips of star
    static TRIANGLE_N = [46, 47, 48, 49, 0, 1, 2, 3, 4]
    static TRIANGLE_NE = [6, 7, 8, 9, 10, 11, 12, 13, 14]
    static TRIANGLE_SE = [16, 17, 18, 19, 20, 21, 22, 23, 24]
    static TRIANGLE_SW = [26, 27, 28, 29, 30, 31, 32, 33, 34]
    static TRIANGLE_NW = [36, 37, 38, 39, 40, 41, 42, 43, 44]
    // LEG_* leds are ordered from inner to outer
    static LEG_1 = [5, 4, 3, 2, 1, 0]
    static LEG_2 = [5, 6, 7, 8, 9, 10]
    static LEG_3 = [15, 14, 13, 12, 11, 10]
    static LEG_4 = [15, 16, 17, 18, 19, 20]
    static LEG_5 = [25, 24, 23, 22, 21, 20]
    static LEG_6 = [25, 26, 27, 28, 29, 30]
    static LEG_7 = [35, 34, 33, 32, 31, 30]
    static LEG_8 = [35, 36, 37, 38, 39, 40]
    static LEG_9 = [45, 44, 43, 42, 41, 40]
    static LEG_10 = [45, 46, 47, 48, 49, 0]

    /** 0:N, 1:NE, 2:SE, 3:SW, 4:NW */
    static LEDS_TREE_STAR = [0, 1, 2, 3, 4]
    /** Left most leg.  Order from top to bottom. */
    static LEDS_TREE_LEG_1 = [5, 6, 7, 8, 9, 10, 11, 12, 13]
    static LEDS_TREE_LEG_2 = [14, 15, 16, 17, 18, 19, 20, 21, 22]
    /** Middle leg.  Order from top to bottom. */
    static LEDS_TREE_LEG_3 = [23, 24, 25, 26, 27, 28, 29, 30, 31]
    static LEDS_TREE_LEG_4 = [32, 33, 34, 35, 36, 37, 38, 39, 40]
    /** Right most leg.  Order from top to bottom. */
    static LEDS_TREE_LEG_5 = [41, 42, 43, 44, 45, 46, 47, 48, 49]

    static BIT_RING_0 = new BitThing([5, 15, 25, 35, 45])

    /**
     * How long a single iteration of a sequence lasts (ms).
     * As the sequence is being built this value will represent the current time/progress
     *   of the sequence where events (e.g., setLeds) will take place.
     * `duration` does not include the time (i.e., blead) which is created from the `rampX` actions.
     */
    #duration = 0
    /** State for pausing and setLeds */
    #solidState: Array<{ leds: Array<number>, color: string, start: number }> = []
    /** State for ramp Up/Down actions */
    #fadeState: Array<{ leds: Array<number>, color: number, start: number, end: number, fun: (n: number) => number }> = []
    /** Index into `#solidState` for the next action which should be taken. */
    #nextSolidStateIndex = 0
    /** The current iteration of the sequence. */
    #currentIteration = 1
    /** The time at which the current iteration started. */
    #currentIterationStartMillis?: number
    /**
     * The amount of time past the end of the last iteration which fading will occur.
     * This time is added to the end of the sequence during the last iteration.
     */
    #maxFadeBleed = 0

    constructor(protected readonly leds: Array<string>, protected iterations = 1) { }

    setLeds(color: string, leds: Array<number>): Sequence {
        this.#solidState.push({ leds, color, start: this.#duration })

        return this
    }

    rampUpV(color: number, leds: Array<number>, duration: number): Sequence {
        this.#fadeState.push({ leds, color, start: this.#duration, end: this.#duration + duration, fun: Math.sin })

        if (duration > this.#maxFadeBleed) {
            this.#maxFadeBleed = duration
        }

        return this
    }

    rampDownV(color: number, leds: Array<number>, duration: number): Sequence {
        this.#fadeState.push({ leds, color, start: this.#duration, end: this.#duration + duration, fun: Math.cos })

        if (duration > this.#maxFadeBleed) {
            this.#maxFadeBleed = duration
        }

        return this
    }

    pause(duration: number): Sequence {
        this.#duration += duration

        this.#maxFadeBleed = Math.max(this.#maxFadeBleed - duration, 0)

        return this
    }

    /**
     * @returns true while animation is ongoing, false when animation is done
     */
    tick(millis: number): boolean {
        if (this.#currentIterationStartMillis === undefined) {
            this.#currentIterationStartMillis = millis
        }

        /** The number of milliseconds since the beginning of the current iteration. */
        const normalizedMillis = millis - this.#currentIterationStartMillis

        // "Fade"
        for (const fade of this.#fadeState) {
            /** Account for fading across iterations. */
            const fadeNormalizedMillis = normalizedMillis < fade.start &&  this.#duration < fade.end  &&  this.#currentIteration > 1 ?
                normalizedMillis + this.#duration :
                normalizedMillis

            if (fadeNormalizedMillis >= fade.start && fadeNormalizedMillis <= fade.end) {
                const radian = ((fadeNormalizedMillis - fade.start) / (fade.end - fade.start)) * Math.PI / 2
                for (const led of fade.leds) {
                    this.leds[led] = `hsl(${fade.color},100%,${fade.fun(radian) * 50}%)`
                }
            }
        }

        // "Solid"
        while (
            this.#nextSolidStateIndex < this.#solidState.length &&
            this.#solidState[this.#nextSolidStateIndex].start <= normalizedMillis &&
            this.#currentIteration <= this.iterations
        ) {
            for (const led of this.#solidState[this.#nextSolidStateIndex].leds) {
                this.leds[led] = this.#solidState[this.#nextSolidStateIndex].color
            }

            ++this.#nextSolidStateIndex
        }

        // Move to next iteration.
        if (normalizedMillis > this.#duration) {
            ++this.#currentIteration
            this.#nextSolidStateIndex = 0
            if (this.#currentIteration <= this.iterations) {  // Don't reset after last iteration for fade.
                this.#currentIterationStartMillis = undefined
            }
        }

        return this.#currentIteration <= this.iterations ||
            (   // Fade after last iteration:
                this.#currentIteration > this.iterations &&
                this.#maxFadeBleed > 0 &&
                normalizedMillis < this.#duration + this.#maxFadeBleed
            )
    }
}
