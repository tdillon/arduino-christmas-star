import { Sequence } from './Sequence.ts'

export class Toggle extends Sequence {
    constructor(leds: Array<string>, iterations = 1, duration = 1000) {
        super(leds, iterations)

        this.setLeds('white', [...Sequence.RING_0, ...Sequence.RING_2, ...Sequence.RING_4])
        this.pause(duration / 2 - 5)
        this.setLeds('black', [...Sequence.RING_0, ...Sequence.RING_2, ...Sequence.RING_4])
        this.setLeds('white', [...Sequence.RING_1, ...Sequence.RING_3, ...Sequence.RING_5])
        this.pause(duration / 2 - 5)
        this.setLeds('black', [...Sequence.RING_1, ...Sequence.RING_3, ...Sequence.RING_5])
        this.pause(10)
    }

}
