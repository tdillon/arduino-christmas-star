import { Sequence } from './Sequence.ts'

export class LegFader extends Sequence {
    constructor(leds: Array<string>) {
        super(leds, 5)

        const legs = [
            Sequence.LEG_7,
            Sequence.LEG_10,
            Sequence.LEG_1,
            Sequence.LEG_4,
            Sequence.LEG_5,
            Sequence.LEG_8,
            Sequence.LEG_9,
            Sequence.LEG_2,
            Sequence.LEG_3,
            Sequence.LEG_6,
        ]

        for (let i = 0; i < legs.length; ++i) {
            this.rampUpV(36 * i, legs[i], 250)
            this.pause(250)
            this.rampDownV(36 * i, legs[i], 250)
            this.pause(250)

        }
    }
}
