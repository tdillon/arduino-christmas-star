/// <reference lib='dom' />
import { ArduinoStarSimulator } from './arduino-star-simulator.ts';
import { ArduinoTreeSimulator } from './arduino-tree-simulator.ts';
import { InAndOut } from './sequencers/InAndOut.ts';
import { Lap } from './sequencers/Lap.ts';
import { LegFader } from './sequencers/LegFader.ts';
import { LegLine } from './sequencers/LegLine.ts';
import { RockPaperScissorsLizardSpock } from './sequencers/RockPaperScissorsLizardSpock.ts';
import { Sequence } from './sequencers/Sequence.ts';
import { SimpleTree } from './sequencers/SimpleTree.ts';
import { Solid } from './sequencers/Solid.ts';
import { Sparkle } from './sequencers/Sparkle.ts';
import { Toggle } from './sequencers/Toggle.ts';
import { Twirl } from './sequencers/Twirl.ts';
import { Star } from './Star.ts';
import { Tree } from './Tree.ts';

const PROGRAM_START_MILLIS = Date.now()

interface SequencerDataStructure {
    shape: typeof Star | typeof Tree,
    simulator: typeof ArduinoStarSimulator | typeof ArduinoTreeSimulator,
    sequencers: Array<typeof Sequence | undefined>
}

const sequencers = ([{
    shape: Star,
    simulator: ArduinoStarSimulator,
    sequencers: [
        InAndOut, Lap, LegFader, LegLine,
        RockPaperScissorsLizardSpock, Solid,
        Sparkle, Toggle, Twirl, undefined
    ]
}, {
    shape: Tree,
    simulator: ArduinoTreeSimulator,
    sequencers: [Solid, SimpleTree, undefined]
}] as Array<SequencerDataStructure>)
    .flatMap(s => s.sequencers.map(sequenceType => ({
        shape: new s.shape(sequenceType),
        simulator: new s.simulator(sequenceType?.name ?? 'Random')
    })))

const loop = () => {
    sequencers.forEach(s => {
        s.shape.tick(Date.now() - PROGRAM_START_MILLIS)
        s.simulator.draw(s.shape.leds)
    })
    requestAnimationFrame(loop)
}

requestAnimationFrame(loop)
