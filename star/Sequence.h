#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <FastLED.h>
#include "SimpleState.h"

struct state_t
{
  byte numLeds;
  int *leds;
  CRGB color;
  int start;
};

class Sequence
{
private:
  unsigned int duration = 0;
  byte iterations = 0;

  state_t state[30];
  // #state: Array<{ leds: Array<number>, color: string, start: number }> = []
  // #continuousState: Array<{ leds: Array<number>, color: number, start: number, end: number, fun: (n: number) => number }> = []
  /** Index into `#state` for the next action which should be taken. */
  byte nextStateIndex = 0;
  byte currentIteration = 1;
  byte simpleStateTop = 0;
  long currentIterationStartMillis = NULL;

protected:
  CRGB *leds;

  static constexpr const int ALL_LEDS[] = {
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
      21,
      22,
      23,
      24,
      25,
      26,
      27,
      28,
      29,
      30,
      31,
      32,
      33,
      34,
      35,
      36,
      37,
      38,
      39,
      40,
      41,
      42,
      43,
      44,
      45,
      46,
      47,
      48,
      49,
  };
  static constexpr const byte ALL_LEDS_SIZE = sizeof(ALL_LEDS) / sizeof(ALL_LEDS[0]);

  /** inner most ring */
  static constexpr const int RING_0[] = {5, 15, 25, 35, 45};
  static constexpr const byte RING_0_SIZE = sizeof(RING_0) / sizeof(RING_0[0]);
  static constexpr const int RING_1[] = {4, 6, 14, 16, 24, 26, 34, 36, 44, 46};
  static constexpr const byte RING_1_SIZE = sizeof(RING_1) / sizeof(RING_1[0]);
  static constexpr const int RING_2[] = {3, 7, 13, 17, 23, 27, 33, 37, 43, 47};
  static constexpr const byte RING_2_SIZE = sizeof(RING_2) / sizeof(RING_2[0]);
  static constexpr const int RING_3[] = {2, 8, 12, 18, 22, 28, 32, 38, 42, 48};
  static constexpr const byte RING_3_SIZE = sizeof(RING_3) / sizeof(RING_3[0]);
  static constexpr const int RING_4[] = {1, 9, 11, 19, 21, 29, 31, 39, 41, 49};
  static constexpr const byte RING_4_SIZE = sizeof(RING_4) / sizeof(RING_4[0]);
  // outer most ring, i.e., tips of star
  static constexpr const int RING_5[] = {0, 10, 20, 30, 40};
  static constexpr const byte RING_5_SIZE = sizeof(RING_5) / sizeof(RING_5[0]);
  static constexpr const int TRIANGLE_N[] = {21, 22, 23, 24, 25, 26, 27, 28, 29};
  static constexpr const byte TRIANGLE_N_SIZE = sizeof(TRIANGLE_N) / sizeof(TRIANGLE_N[0]);
  static constexpr const int TRIANGLE_NE[] = {31, 32, 33, 34, 35, 36, 37, 38, 39 };
  static constexpr const byte TRIANGLE_NE_SIZE = sizeof(TRIANGLE_NE) / sizeof(TRIANGLE_NE[0]);
  static constexpr const int TRIANGLE_SE[] = {41,42,43,44,45,46,47,48,49};
  static constexpr const byte TRIANGLE_SE_SIZE = sizeof(TRIANGLE_SE) / sizeof(TRIANGLE_SE[0]);
  static constexpr const int TRIANGLE_SW[] = {1,2,3,4,5,6,7,8,9};
  static constexpr const byte TRIANGLE_SW_SIZE = sizeof(TRIANGLE_SW) / sizeof(TRIANGLE_SW[0]);
  static constexpr const int TRIANGLE_NW[] = {11,12,13,14,15,16,17,18,19};
  static constexpr const byte TRIANGLE_NW_SIZE = sizeof(TRIANGLE_NW) / sizeof(TRIANGLE_NW[0]);
  // LEG_* leds are ordered from inner to outer
  static constexpr const int LEG_1[] = {5, 4, 3, 2, 1, 0};
  static constexpr const byte LEG_1_SIZE = sizeof(LEG_1) / sizeof(LEG_1[0]);
  static constexpr const int LEG_2[] = {5, 6, 7, 8, 9, 10};
  static constexpr const byte LEG_2_SIZE = sizeof(LEG_2) / sizeof(LEG_2[0]);
  static constexpr const int LEG_3[] = {15, 14, 13, 12, 11, 10};
  static constexpr const byte LEG_3_SIZE = sizeof(LEG_3) / sizeof(LEG_3[0]);
  static constexpr const int LEG_4[] = {15, 16, 17, 18, 19, 20};
  static constexpr const byte LEG_4_SIZE = sizeof(LEG_4) / sizeof(LEG_4[0]);
  static constexpr const int LEG_5[] = {25, 24, 23, 22, 21, 20};
  static constexpr const byte LEG_5_SIZE = sizeof(LEG_5) / sizeof(LEG_5[0]);
  static constexpr const int LEG_6[] = {25, 26, 27, 28, 29, 30};
  static constexpr const byte LEG_6_SIZE = sizeof(LEG_6) / sizeof(LEG_6[0]);
  static constexpr const int LEG_7[] = {35, 34, 33, 32, 31, 30};
  static constexpr const byte LEG_7_SIZE = sizeof(LEG_7) / sizeof(LEG_7[0]);
  static constexpr const int LEG_8[] = {35, 36, 37, 38, 39, 40};
  static constexpr const byte LEG_8_SIZE = sizeof(LEG_8) / sizeof(LEG_8[0]);
  static constexpr const int LEG_9[] = {45, 44, 43, 42, 41, 40};
  static constexpr const byte LEG_9_SIZE = sizeof(LEG_9) / sizeof(LEG_9[0]);
  static constexpr const int LEG_10[] = {45, 46, 47, 48, 49, 0};
  static constexpr const byte LEG_10_SIZE = sizeof(LEG_10) / sizeof(LEG_10[0]);

public:
  Sequence(CRGB leds[], byte iterations);
  void setLeds( const int *ledIdx, byte numLeds, CRGB color);
  void pause(unsigned int duration);
  bool tick(long millis);
};

#endif