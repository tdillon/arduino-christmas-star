#ifndef STAR_H
#define STAR_H

#include <FastLED.h>
#include "Constants.h"
#include "Sequence.h"
#include "Solid.h"
#include "RockPaperScissorsLizardSpock.h"

class Star
{
private:
  bool isTransitionSequence;
  CRGB *leds;
  Sequence *sequence;

public:
  Star(CRGB leds[]);
  void tick(long millis);
};

#endif