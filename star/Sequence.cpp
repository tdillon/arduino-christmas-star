#include "Sequence.h"

constexpr const int Sequence::ALL_LEDS[];
constexpr const byte Sequence::ALL_LEDS_SIZE;
constexpr const int Sequence::RING_0[];
constexpr const byte Sequence::RING_0_SIZE;
constexpr const int Sequence::RING_1[];
constexpr const byte Sequence::RING_1_SIZE;
constexpr const int Sequence::RING_2[];
constexpr const byte Sequence::RING_2_SIZE;
constexpr const int Sequence::RING_3[];
constexpr const byte Sequence::RING_3_SIZE;
constexpr const int Sequence::RING_4[];
constexpr const byte Sequence::RING_4_SIZE;
constexpr const int Sequence::RING_5[];
constexpr const byte Sequence::RING_5_SIZE;
constexpr const int Sequence::TRIANGLE_N[];
constexpr const byte Sequence::TRIANGLE_N_SIZE;
constexpr const int Sequence::TRIANGLE_NE[];
constexpr const byte Sequence::TRIANGLE_NE_SIZE;
constexpr const int Sequence::TRIANGLE_SE[];
constexpr const byte Sequence::TRIANGLE_SE_SIZE;
constexpr const int Sequence::TRIANGLE_SW[];
constexpr const byte Sequence::TRIANGLE_SW_SIZE;
constexpr const int Sequence::TRIANGLE_NW[];
constexpr const byte Sequence::TRIANGLE_NW_SIZE;
constexpr const int Sequence::LEG_1[];
constexpr const byte Sequence::LEG_1_SIZE;
constexpr const int Sequence::LEG_2[];
constexpr const byte Sequence::LEG_2_SIZE;
constexpr const int Sequence::LEG_3[];
constexpr const byte Sequence::LEG_3_SIZE;
constexpr const int Sequence::LEG_4[];
constexpr const byte Sequence::LEG_4_SIZE;
constexpr const int Sequence::LEG_5[];
constexpr const byte Sequence::LEG_5_SIZE;
constexpr const int Sequence::LEG_6[];
constexpr const byte Sequence::LEG_6_SIZE;
constexpr const int Sequence::LEG_7[];
constexpr const byte Sequence::LEG_7_SIZE;
constexpr const int Sequence::LEG_8[];
constexpr const byte Sequence::LEG_8_SIZE;
constexpr const int Sequence::LEG_9[];
constexpr const byte Sequence::LEG_9_SIZE;
constexpr const int Sequence::LEG_10[];
constexpr const byte Sequence::LEG_10_SIZE;

Sequence::Sequence(CRGB leds[], byte iterations)
{
  this->leds = leds;
  this->iterations = iterations;
}

void Sequence::setLeds(const int *ledIdx, byte numLeds, CRGB color)
{
  state_t x;
  x.color = color;
  x.start = this->duration;
  x.numLeds = numLeds;

  x.leds = ledIdx;

  // x.leds[0] = ledIdx[0];
  // x.leds[1] = ledIdx[1];
  // x.leds[2] = ledIdx[2];
  // x.leds[3] = ledIdx[3];
  // x.leds[4] = ledIdx[4];
  // x.leds[5] = ledIdx[5];

  state[this->simpleStateTop++] = x;
}

void Sequence::pause(unsigned int duration)
{
  this->duration += duration;
}

bool Sequence::tick(long millis)
{
  if (this->currentIterationStartMillis == NULL)
  {
    this->currentIterationStartMillis = millis;
  }

  while (this->nextStateIndex < simpleStateTop &&
         (this->state[this->nextStateIndex].start + this->currentIterationStartMillis) <= millis &&
         this->currentIteration <= this->iterations)
  {
    state_t currentState = this->state[this->nextStateIndex];

    // TODO HACK, ugh, how to deal with arrays/pointers in c++
    for (size_t i = 0; i < currentState.numLeds; ++i)
    {
      this->leds[currentState.leds[i]] = currentState.color;
    }

    ++this->nextStateIndex;
  }

  if (millis > (this->currentIterationStartMillis + this->duration))
  {
    ++this->currentIteration;
    this->currentIterationStartMillis = NULL;
    this->nextStateIndex = 0;
  }

  return this->currentIteration <= this->iterations;
}