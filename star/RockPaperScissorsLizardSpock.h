#ifndef ROCK_PAPER_SCISSORS_LIZARD_SPOCK_H
#define ROCK_PAPER_SCISSORS_LIZARD_SPOCK_H

#include "Sequence.h"

class RockPaperScissorsLizardSpock : public Sequence
{
public:
    RockPaperScissorsLizardSpock(CRGB leds[]);
};

#endif