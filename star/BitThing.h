#ifndef BITTHING_H
#define BITTHING_H

#include <Arduino.h>

class BitThing {
private:
  unsigned long a;  //  0 - 24
  unsigned long b;  // 25 - 49

public:
  BitThing();
  bool isIndexSet(byte i);
};

#endif
