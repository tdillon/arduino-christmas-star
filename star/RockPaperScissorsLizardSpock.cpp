#include "RockPaperScissorsLizardSpock.h"

RockPaperScissorsLizardSpock::RockPaperScissorsLizardSpock(CRGB leds[])
    : Sequence(leds, 1)
{
    const int *winner = TRIANGLE_N;
    const int *looser = TRIANGLE_SE;

    setLeds(TRIANGLE_N, TRIANGLE_N_SIZE, CRGB::Orange);
    pause(350);
    setLeds(TRIANGLE_NE, TRIANGLE_NE_SIZE, CRGB::Orange);
    setLeds(TRIANGLE_N, TRIANGLE_N_SIZE, CRGB::Black);
    pause(350);
    setLeds(TRIANGLE_SE, TRIANGLE_SE_SIZE, CRGB::Orange);
    setLeds(TRIANGLE_NE, TRIANGLE_NE_SIZE, CRGB::Black);
    pause(350);
    setLeds(TRIANGLE_SW, TRIANGLE_SW_SIZE, CRGB::Orange);
    setLeds(TRIANGLE_SE, TRIANGLE_SE_SIZE, CRGB::Black);
    pause(350);
    setLeds(TRIANGLE_NW, TRIANGLE_NW_SIZE, CRGB::Orange);
    setLeds(TRIANGLE_SW, TRIANGLE_SW_SIZE, CRGB::Black);
    pause(350);
    setLeds(TRIANGLE_NW, TRIANGLE_NW_SIZE, CRGB::Black);
    pause(350);
    setLeds(winner, TRIANGLE_N_SIZE, CRGB::Blue);
    setLeds(looser, TRIANGLE_SE_SIZE, CRGB::Blue);
    pause(2000);
    setLeds(winner, TRIANGLE_N_SIZE, CRGB::Black);
    setLeds(looser, TRIANGLE_SE_SIZE, CRGB::Black);
    pause(500);
    setLeds(winner, TRIANGLE_N_SIZE, CRGB::Green);
    setLeds(looser, TRIANGLE_SE_SIZE, CRGB::Red);
    pause(1000);
    setLeds(winner, TRIANGLE_N_SIZE, CRGB::Black);
    pause(500);
    setLeds(winner, TRIANGLE_N_SIZE, CRGB::Green);
    pause(1000);
    setLeds(winner, TRIANGLE_N_SIZE, CRGB::Black);
    pause(500);
    setLeds(winner, TRIANGLE_N_SIZE, CRGB::Green);
    pause(1000);
}
