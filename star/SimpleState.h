#ifndef SIMPLESTATE_H
#define SIMPLESTATE_H

#include <FastLED.h>
#include "BitThing.h"

struct SimpleState {
public:
  BitThing leds;
  CRGB color;
  unsigned int start;
  SimpleState();
};

#endif