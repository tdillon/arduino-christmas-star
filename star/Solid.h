#ifndef SOLID_H
#define SOLID_H

#include "Sequence.h"

class Solid : public Sequence {
public:
  Solid(CRGB leds[]);
  Solid(CRGB leds[], byte iterations, unsigned int duration, CRGB color);
};

#endif