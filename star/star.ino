#include <FastLED.h>
#include "Constants.h"
#include "Star.h"

#define DATA_PIN 6

CRGB leds[NUM_LEDS];

Star star(leds);

void setup()
{
  Serial.begin(115200);
  FastLED.addLeds<WS2811, DATA_PIN, RGB>(leds, NUM_LEDS);
}

void loop()
{
  star.tick(millis());
  FastLED.show();
}
