#include "BitThing.h"

BitThing::BitThing() {
  a = 0b00011110;
  // todo
}

bool BitThing::isIndexSet(byte i) {
  return (((1 << i) & a) > 0) || (((1 << i) & b) > 0);
}

// /**
//  * Could a class/struct like this be used on the arduino be used
//  * to store the led indices using a fixed 8 bytes instead of an
//  * array of bytes (of between 1 and 50 bytes)?
//  0b0000'0100'0100'0001
//  */
// class BitThing {
//     #a = 0
//     #b = 0

//     constructor(i: Array<number>) {
//         this.#setIndices(i)
//     }

//     #setIndices(i: Array<number>) {
//         for (const a of i) {
//             if (a < 25) {
//                 this.#a |= (1 << a)
//             } else {
//                 this.#b |= (1 << a)
//             }
//         }
//     }

//     isIndexSet(i: number): boolean {
//         return (((1 << i) & this.#a) > 0) || (((1 << i) & this.#b) > 0)
//     }
// }