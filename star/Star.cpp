#include "Star.h"

Star::Star(CRGB leds[])
{
  isTransitionSequence = false;
  this->leds = leds;
}

void Star::tick(long millis)
{
  if (sequence)
  {
    if (!(sequence->tick(millis)))
    {
      delete sequence;
      sequence = NULL;
      if (isTransitionSequence)
      {
        isTransitionSequence = false;
      }
      else
      {
        sequence = new Solid(leds);
        isTransitionSequence = true;
      }
    }
  }
  else
  {
    //  TODO randomly pick sequence
    // sequence = new Solid(leds, random(1, 5), random(500, 5000), CHSV(random(0, 255), 187, 100));
    sequence = new RockPaperScissorsLizardSpock(leds);
  }
}