#include "Solid.h"

Solid::Solid(CRGB leds[])
    : Sequence(leds, 1)
{
  setLeds(ALL_LEDS, ALL_LEDS_SIZE, CRGB::Black);
  pause(500);
}

Solid::Solid(CRGB leds[], byte iterations, unsigned int duration, CRGB color)
    : Sequence(leds, iterations)
{
  setLeds(ALL_LEDS, ALL_LEDS_SIZE, color);
  pause(duration - 250);
  setLeds(ALL_LEDS, ALL_LEDS_SIZE, CRGB::Black);
  pause(250);
}
