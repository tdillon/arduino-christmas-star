# Arduino Christmas Star

Arduino code for controlling a strand of 50 ALITOVE WS2811 RGBs in the shape of a star for a Christmas decoration.

## Development

### Environment Setup

This needs done once to get your computer setup to do Arduino development using VS Code.
Here's what I did.

- Download Arduiono IDE (`Linux 64 bits`) from https://www.arduino.cc/en/main/software.
- Extract the `tar.xz` to a well known location.
- Install the Arduino extension for VS Code (https://github.com/microsoft/vscode-arduino).
- Update the `arduino.path` in VS Code (see instructions in link above) to point to the extracted folder from above (e.g., `/home/<username>/Downloads/arduino-x.y.z/`)

### One Time Repo Setup

This will need done once to get your repo setup for development.

- Clone the repo
- Open the repo in VS Code
- `ctrl+shift+p` -> `Arduino: Library Manager`
  - Filter your search...  FastLED
  - Install
  - Include (But remove all the headers added except `#include <FastLED.h>`)
- `ctrl+shift+p` -> `Arduino: Initialize`
- `ctrl+shift+p` -> `Arduino: Select Serial Port`

Here is what my `.vscode/arduino.json` file looks like at this point (I added the output property).

```json
{
    "sketch": "app.ino",
    "board": "arduino:avr:uno",
    "port": "/dev/ttyACM0",
    "output": "build"
}
```

Here are the important properties in my `.vscode/c_cpp_properties.json` file to get intellisense working.

```json
...

"includePath": [
    "<path to home dir>/Arduino/**",
    "<path to home dir>/Downloads/arduino-1.8.10/hardware/arduino/avr/**",
    "<path to home dir>/Downloads/arduino-1.8.10/tools/**",
    "<path to home dir>/Downloads/arduino-1.8.10/hardware/tools/**",
    "<path to home dir>/Downloads/arduino-1.8.10/hardware/arduino/avr/cores/arduino"
],
"forcedInclude": [
    "<path to home dir>/Downloads/arduino-1.8.10/hardware/arduino/avr/cores/arduino/Arduino.h"
],

...
```

### Ongoing Changes
- If you get permission issues writing to the Arduino run `sudo chmod a+rw /dev/ttyACM0`
- Make changes to sketch
- `ctrl+shift+p` -> `Arduino: Upload`
